<?php
$TOKEN = "xxx";

if(isset($_GET['city_id'])){
    $URL_CITY = "https://restapi.infoflot.com/cities/".$_GET['city_id']."?key=".$TOKEN;

    $city = get_url_content($URL_CITY);

    echo "<h1>".$city['name']."</h1>";
    echo "<img src='".$city['photos'][0]['filename']."'><br>";
    echo "<p>".$city['description']."</p>";
    exit;
}

$URL_CITIES = "https://restapi.infoflot.com/cities?key=".$TOKEN;

$cities = get_url_content($URL_CITIES);

$citiesArray = $cities['data'];

foreach ($citiesArray as $city) {
    echo "<a href='index.php?city_id=".$city['id']."'>".$city['name']."</a><br>";
}


function get_url_content($url){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $out = curl_exec($curl);
    curl_close($curl);

    return json_decode($out, true);
}
?>